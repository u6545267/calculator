package application;

public class Calculator {

    public static void main(String[] args) {
//    System.out.println("3-2="+run(3,2,"-"));
//    System.out.println("3+2="+run(3,2,"+"));
//    System.out.println("3*2="+run(3,2,"*"));
//    System.out.println("3/2="+run(3,2,"/"));
    }

    static float run(float a, float b, String operator) {
        float result = 0;
        switch (operator) {
            case "+":
                result = a + b;
                break;
            case "-":
                result = a - b;
                break;
            case "*":
                result = a * b;
                break;
            case "/":
                result = a / b;
                break;
            default:
                break;
        }
        return result;
    }
}
