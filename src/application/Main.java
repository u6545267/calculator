package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
    // initialize previousNum so it won't be displayed as 'null' later
    private static String previousNum = "";
    private static Float num1;
    private static Float num2;
    private static Float ans;
    private static boolean ifStart = true;
    private static String operator;

    @Override
    public void start(Stage stage) {
        try {
            stage.setResizable(false);

            VBox vbox = new VBox();
            Scene scene = new Scene(vbox, 300, 400);

            StackPane stackpane = new StackPane();
            vbox.getChildren().add(stackpane);

            Label screen = new Label("Go!");
            stackpane.getChildren().add(screen);
            screen.setAlignment(Pos.BOTTOM_RIGHT);

            HBox hbBtn1 = new HBox(10);
            HBox hbBtn2 = new HBox(10);
            HBox hbBtn3 = new HBox(10);
            HBox hbBtn4 = new HBox(10);
            vbox.getChildren().addAll(hbBtn1, hbBtn2, hbBtn3, hbBtn4);
            vbox.setSpacing(10);
            vbox.setAlignment(Pos.CENTER);
            vbox.setPadding(new Insets(25, 25, 25, 25));

            Button btn7 = new Button("7");
            Button btn8 = new Button("8");
            Button btn9 = new Button("9");
            Button times = new Button("*");
            hbBtn1.getChildren().addAll(btn7, btn8, btn9, times);

            Button btn4 = new Button("4");
            Button btn5 = new Button("5");
            Button btn6 = new Button("6");
            Button divide = new Button("/");
            hbBtn2.getChildren().addAll(btn4, btn5, btn6, divide);

            Button btn1 = new Button("1");
            Button btn2 = new Button("2");
            Button btn3 = new Button("3");
            Button plus = new Button("+");
            hbBtn3.getChildren().addAll(btn1, btn2, btn3, plus);

            Button btn0 = new Button("0");
            btn0.setId("btn0");
            Button equal = new Button("=");
            Button minus = new Button("-");
            hbBtn4.getChildren().addAll(btn0, equal, minus);

            // implementation for numberHandler
            try {
                final EventHandler<ActionEvent> numberHandler = e -> {
                    // Check the state of the calculation
                    // If the calculation is on the start:
                    // a) reset the screen to empty,
                    // b) change the state of the calculation
                    if (ifStart) {
                        screen.setText("");
                        ifStart = false;
                    }
                    // Get value of the number on the clicked button
                    Object node = e.getSource();
                    Button temp = (Button) node;
                    // Show the clicked number following previously clicked number
                    screen.setText(previousNum + temp.getText());
                    previousNum = screen.getText();
                };
                btn0.setOnAction(numberHandler);
                btn1.setOnAction(numberHandler);
                btn2.setOnAction(numberHandler);
                btn3.setOnAction(numberHandler);
                btn4.setOnAction(numberHandler);
                btn5.setOnAction(numberHandler);
                btn6.setOnAction(numberHandler);
                btn7.setOnAction(numberHandler);
                btn8.setOnAction(numberHandler);
                btn9.setOnAction(numberHandler);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // implementation for operatorHandler
            try {
                operator = null;
                final EventHandler<ActionEvent> operatorHandler = e -> {
                    if (ifStart) {
                        // If it is on the start --> do nothing
                    } else {
                        // Get the text of the clicked button
                        Object node = e.getSource();
                        Button temp = (Button) node;
                        // Check the value of the operator
                        if (!temp.getText().equals("=")) {
                            // get the operator
                            operator = temp.getText();
                            // get the number1 from the screen
                            num1 = Float.parseFloat(screen.getText());
                            // reset the screen to blank, waiting for new number
                            screen.setText("");
                            // reset previousNum
                            previousNum = "";
                        } else {
                            // check if there is already an operator stored
                            if (operator == null) {
                                // if it is, do nothing
                            } else if (operator != null) {
                                num2 = Float.parseFloat(screen.getText());
                                // call calculate method to compute the output
                                ans = Calculator.run(num1, num2, operator);
                                // show the output result on the screen
                                screen.setText(ans.toString());
                                // reset operator to empty
                                operator = null;
                                // reset state of calculator to the start
                                ifStart = true;
                                previousNum = "";
                                num1 = null;
                                num2 = null;
                            }
                        }
                    }
                };
                times.setOnAction(operatorHandler);
                plus.setOnAction(operatorHandler);
                divide.setOnAction(operatorHandler);
                minus.setOnAction(operatorHandler);
                equal.setOnAction(operatorHandler);
            } catch (Exception e) {
                e.printStackTrace();
            }
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
